package ru.pcs.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.pcs.web.models.Product;
import ru.pcs.web.repositories.ProductsRepository;

@Controller
public class ProductsController {

    @Autowired
    private ProductsRepository productsRepository;

    @PostMapping("/products")
    public String addProduct(@RequestParam("description") String description,
                             @RequestParam("price") String price,
                             @RequestParam("count") String count) {
        Product product = Product.builder()
                .description(description)
                .price(Integer.parseInt(price))
                .count(Integer.parseInt(count))
                .build();
        productsRepository.save(product);
        return "redirect:/product_add.html";
    }
}

create table product
(
  id          serial primary key,
  description varchar(20),
  price       integer,
  count       integer
);

insert into product (description, price, count)
values ('Lamp', 350, 7);
insert into product (description, price, count)
values ('Table', 1700, 12);
insert into product (description, price, count)
values ('Chair', 800, 4);
insert into product (description, price, count)
values ('TV', 5000, 10);
insert into product (description, price, count)
values ('Pillow', 170, 18);

create table client
(
  id         serial primary key,
  first_name varchar(20),
  last_name  varchar(20)
);

insert into client (first_name, last_name)
values ('Андрей', 'Смирнов');
insert into client (first_name, last_name)
values ('Илья', 'Ремизов');
insert into client (first_name, last_name)
values ('Василий', 'Исимбаев');
insert into client (first_name, last_name)
values ('Давид', 'Ансимов');
insert into client (first_name, last_name)
values ('Якуб', 'Ульянов');


create table "order"
(
  id         serial primary key,
  date       varchar(20),
  id_product integer,
  foreign key (id_product) references product(id),
  id_client integer,
  foreign key (id_client) references client(id)

);


